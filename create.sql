use gssdb;

create table Project(
	ProjectID int auto_increment,
  ProjectName varchar(100),
  primary key(ProjectID)
  );

create table Designation(
	DesignationID int auto_increment,
  DesignationName varchar(200),
  primary key(DesignationID)
  );
    
create table Department(
	DepartmentID int auto_increment,
    DepartmentName varchar(200),
    primary key(DepartmentID)
    );
    
create table Role(
	  RoleID int auto_increment,
    RoleName varchar(20),
    primary key(RoleID)
	);
    
insert into Role(RoleName) values
  ('Super User'),
  ('Admin'),
  ('Staff'),
  ('Employee');
    
create table Employee(
	  EmployeeID int auto_increment,
	  Email varchar(100),
    Password varchar(100),
    Name varchar(200),
    DateOfBirth date,
    CompanyNumber varchar(10),
    EmployeeNumber varchar(10),
    NIC varchar(12),
    ContactNumber char(10), 
    EmergencyContactNumber char(10),
	  Address varchar(300),
    DateOfHire date,
    DateOfJoin date,
    Role int,
    RegisterDate timestamp default current_timestamp, 
    ReportTo int,
    ProjectID int,
    DepartmentID int,
    DesignationID int,
    primary key(EmployeeID, Email),
    foreign key(ReportTo) references Employee(EmployeeID),
    foreign key(ProjectID) references Project(ProjectID),
    foreign key(DepartmentID) references Department(DepartmentID),
    foreign key(DesignationID) references Designation(DesignationID)
    );
    
    