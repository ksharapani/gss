from flask import Flask, redirect, url_for, session, logging, request, render_template
from flask.helpers import flash
from flaskext.mysql import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from models import AddProject, RegisterEmployee, AddDesignation, AddDepartment

app = Flask(__name__)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'gss@123'
app.config['MYSQL_DATABASE_DB'] = 'gssdb'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/dashboard')
def dashboard():
    conn = mysql.connect()
    data_cursor = conn.cursor()
    data_cursor.execute("SELECT * FROM Project;")
    project_count = data_cursor.fetchall()
    data_cursor.execute("SELECT * FROM Department;")
    department_count = data_cursor.fetchall()
    data_cursor.execute("SELECT * FROM Designation;")
    designation_count = data_cursor.fetchall()
    data_cursor.execute("SELECT * FROM Employee;")
    employee_count = data_cursor.fetchall()
    data_cursor.close()

    return render_template('dashboard.html', project_count=project_count, department_count=department_count,
                           designation_count=designation_count, employee_count=employee_count)


@app.route('/dashboard/projects', methods=['GET', 'POST'])
def projects():
    conn = mysql.connect()
    data_cursor = conn.cursor()
    data_cursor.execute("SELECT * FROM Project;")
    project_set = data_cursor.fetchall()
    data_cursor.close()

    form = AddProject(request.form)

    if request.method == 'POST' and form.validate():
        project_name = form.project_name.data

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("INSERT INTO Project(ProjectName) VALUES (%s);", project_name)
        conn.commit()
        cursor.close()

        flash('Project added', 'success')

        return redirect(url_for('projects'))

    return render_template('dashboard/projects.html', form=form, project_set=project_set)


@app.route('/dashboard/designations', methods=['GET', 'POST'])
def designations():
    conn = mysql.connect()
    data_cursor = conn.cursor()
    data_cursor.execute("SELECT * FROM Designation;")
    designation_set = data_cursor.fetchall()
    data_cursor.close()

    form = AddDesignation(request.form)

    if request.method == 'POST' and form.validate():
        designation_name = form.designation_name.data

        cursor = conn.cursor()
        cursor.execute("INSERT INTO Designation(DesignationName) VALUES (%s);", designation_name)
        conn.commit()
        cursor.close()

        flash('Designation added', 'success')

        return redirect(url_for('designations'))

    return render_template('dashboard/designations.html', form=form, designation_set=designation_set)


@app.route('/dashboard/departments', methods=['GET', 'POST'])
def departments():
    conn = mysql.connect()
    data_cursor = conn.cursor()
    data_cursor.execute("SELECT * FROM Department;")
    department_set = data_cursor.fetchall()
    data_cursor.close()

    form = AddDepartment(request.form)

    if request.method == 'POST' and form.validate():
        department_name = form.department_name.data

        cursor = conn.cursor()
        cursor.execute("INSERT INTO Department(DepartmentName) VALUES (%s);", department_name)
        conn.commit()
        cursor.close()

        flash('Department added', 'success')

        return redirect(url_for('departments'))

    return render_template('dashboard/departments.html', form=form, department_set=department_set)


@app.route('/dashboard/add_member', methods=['GET', 'POST'])
def add_member():
    conn = mysql.connect()
    data_cursor = conn.cursor()
    data_cursor.execute("SELECT * FROM Project;")
    project_count = data_cursor.fetchall()
    data_cursor.execute("SELECT * FROM Department;")
    department_count = data_cursor.fetchall()
    data_cursor.execute("SELECT * FROM Designation;")
    designation_count = data_cursor.fetchall()
    data_cursor.execute("SELECT * FROM Employee;")
    employee_count = data_cursor.fetchall()
    data_cursor.close()

    form = RegisterEmployee(request.form)

    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        date_of_birth = form.date_of_birth.data
        company_number = form.company_number.data
        employee_number = form.employee_number.data
        nic = form.nic.data
        contact_number = form.contact_number.data
        emergency_contact_number = form.emergency_contact_number.data
        address = form.address.data
        date_of_hire = form.date_of_hire.data
        date_of_join = form.date_of_join.data
        project_id = request.form['project']
        report_to = None
        department_id = 1
        designation_id = 1
        password = '1234'

        cursor = conn.cursor()
        cursor.execute("INSERT INTO Employee(Email, Password, Name, DateOfBirth, CompanyNumber, EmployeeNumber, NIC, "
                       "ContactNumber, EmergencyContactNumber, Address, DateOfHire, DateOfJoin, Role, ReportTo, "
                       "ProjectID, DepartmentID, DesignationID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                       "%s, %s, %s, %s, %s, %s);", (email, password, name, date_of_birth, company_number,
                                                    employee_number, nic, contact_number, emergency_contact_number,
                                                    address, date_of_hire, date_of_join, 1, report_to, project_id,
                                                    department_id, designation_id))
        conn.commit()
        cursor.close()

        return redirect(url_for('add_member'))

    return render_template('dashboard/add_member.html', form=form, project_count=project_count,
                           department_count=department_count, designation_count=designation_count)


if __name__ == '__main__':
    app.secret_key = 'gss@123'
    app.run(debug=True)
