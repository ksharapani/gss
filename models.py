from wtforms import Form, StringField, TextAreaField, PasswordField, validators, DateField


class AddProject(Form):
    project_name = StringField('Project', [validators.required(), validators.length(max=200)])


class AddDesignation(Form):
    designation_name = StringField('Designation', [validators.required(), validators.length(max=200)])


class AddDepartment(Form):
    department_name = StringField('Department', [validators.required(), validators.length(max=200)])


class RegisterEmployee(Form):
    email = StringField('Email', [validators.email(message=None)])
    name = StringField('Name', [validators.length(min=5, max=200)])
    date_of_birth = DateField('Date of Birth')
    company_number = StringField('Company Number')
    employee_number = StringField('Employee Number')
    nic = StringField('NIC', [validators.length(min=10, max=12)])
    contact_number = StringField('Contact Number')
    emergency_contact_number = StringField('Emergency Contact Number')
    address = StringField('Address')
    date_of_hire = DateField('Date of Hire')
    date_of_join = DateField('Date of Join')
